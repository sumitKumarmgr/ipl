let matches = require('../data/matches.json');
let deliveries = require('../data/deliveries.json');
const fs = require('fs');
const iplFunctions = require('./ipl');

const iplYearBook = iplFunctions.matchesPerYear(matches);
filePath1 = "../public/output/matchesPerYear.json"
writeJson(filePath1, iplYearBook);

const iplResultBook = iplFunctions.noOfWinsPerYear(matches);
filePath2 = "../public/output/noOfWinsPerYear.json"
writeJson(filePath2, iplResultBook);

const extraRuns = iplFunctions.extraRuns2016(deliveries, matches)
filePath3 = "../public/output/extraRuns2016.json"
writeJson(filePath3, extraRuns);

const topEconomyBowlersIpl = iplFunctions.economicalBowlers(deliveries, matches);
filePath4 = "../public/output/economicalBowlers.json"
writeJson(filePath4, topEconomyBowlersIpl);

const winsMatchAndToss = iplFunctions.noOfTossAndMatches(matches);
filePath5 = "../public/output/noOfTossAndMatches.json"
writeJson(filePath5,winsMatchAndToss );

const manOfTheMatchsPerYear = iplFunctions.playerOfTheMatch(matches);
filePath6 = "../public/output/playerOfTheMatch.json"
writeJson(filePath6,manOfTheMatchsPerYear);


function writeJson(filePath, FileName) {
   fs.writeFile(filePath, JSON.stringify(FileName), (err) => {
      if (err) console.log(err)
   })
}
