let matches = require('../data/matches.json');
let deliveries = require('../data/deliveries.json');
const fs = require('fs');
const { Console } = require('console');

//Number of matches played per year for all the years in IPL.
function matchesPerYear(matches) {
  let iplMatchesPerYear = matches.reduce((iplMatchesPerYear, currentValue) => {
    if (iplMatchesPerYear.hasOwnProperty(currentValue.season)) {
      iplMatchesPerYear[currentValue.season] += 1;
    } else {
      iplMatchesPerYear[currentValue.season] = 1;
    }
    return iplMatchesPerYear;
  }, {})
  return iplMatchesPerYear;
}
let iplMatchesPerYear = matchesPerYear(matches);

//Number of matches won per team per year in IPL.
function noOfWinsPerYear(matches) {
  let iplMatcheResults = matches.reduce((iplMatcheResults, currentMatch) => {
    if (iplMatcheResults.hasOwnProperty(currentMatch.season)) {
      if (iplMatcheResults[currentMatch.season].hasOwnProperty(currentMatch.winner)) {
        iplMatcheResults[currentMatch.season][currentMatch.winner] += 1;
      } else {
        if (currentMatch.winner != '') {
          iplMatcheResults[currentMatch.season][currentMatch.winner] = 1;
        }
      }
    }
    else {
      iplMatcheResults[currentMatch.season] = {};
    }
    return iplMatcheResults;
  }, {});

  return iplMatcheResults;
}

const iplMatcheResults = noOfWinsPerYear(matches);

//Extra runs conceded per team in the year 2016
function extraRuns2016(deliveries, matches) {

  let idOf2016 = matches.filter((match) => match.season == 2016)
    .map((match) => parseInt(match.id));

  let extraRuns = deliveries.reduce((extraRuns, currentBall) => {
    let matchId = parseInt(currentBall.match_id);
    if (idOf2016.includes(matchId)) {
      if (extraRuns.hasOwnProperty(currentBall.bowling_team)) {
        extraRuns[currentBall.bowling_team] += parseInt(currentBall.extra_runs);
      } else {
        extraRuns[currentBall.bowling_team] = parseInt(currentBall.extra_runs);
      }
    }
    return extraRuns;
  }, {})
  return extraRuns;
}

const extraRuns = extraRuns2016(deliveries, matches);

//Top 10 economical bowlers in the year 2015
function economicalBowlers(deliveries, matches) {

  let idOf2015 = matches.filter((match) => match.season == 2015)
    .map((match) => parseInt(match.id));

  let allBowlers = deliveries.reduce((allBowlers, currentBall) => {

    let matchId = parseInt(currentBall.match_id)
    if (idOf2015.includes(matchId)) {
      if (allBowlers.hasOwnProperty(currentBall.bowler)) {
        allBowlers[currentBall.bowler].balls += 1;
        allBowlers[currentBall.bowler].runs += parseInt(currentBall.total_runs);
        let run = allBowlers[currentBall.bowler].runs;
        let ball = allBowlers[currentBall.bowler].balls;
        allBowlers[currentBall.bowler].economy = (run / (ball / 6)).toFixed(2);
      } else {
        allBowlers[currentBall.bowler] = { "runs": parseInt(currentBall.total_runs), "balls": 1, "economy": 0 };
      }
    }
    return allBowlers;
  }, {})

  const contiansOnlyEconomy = [];
  for (let bowlerName in allBowlers) {
    if (allBowlers[bowlerName].economy <= 8) {
      contiansOnlyEconomy.push(allBowlers[bowlerName].economy);
    }
  }

  contiansOnlyEconomy.sort();

  const top10EconomyBowlers = {};
  for (let index = 0; index < 10; index++) {
    for (key in allBowlers) {
      if (allBowlers[key].economy == contiansOnlyEconomy[index]) {
        top10EconomyBowlers[key] = { economyRate: allBowlers[key].economy };
      }
    }
  }

  return top10EconomyBowlers;

}

const topEconomyBowlersOfIpl = economicalBowlers(deliveries, matches);

//Find the number of times each team won the toss and also won the match
function noOfTossAndMatches(matches) {
  
  let resultYearBook = matches.reduce((resultYearBook, match) => {
    if (resultYearBook.hasOwnProperty(match.season)) {
      if (match.toss_winner == match.winner) {
        resultYearBook[match.season] += 1;
      }
    }else{
      if (match.toss_winner == match.winner) {
      resultYearBook[match.season] = 1;
      }else{ 
      resultYearBook[match.season] = 0;
      }
    }
    return resultYearBook;
  }, {});

  return resultYearBook;
}

const winMatchesAndToss = noOfTossAndMatches(matches);

//Find a player who has won the highest number of Player of the Match awards for each season
function playerOfTheMatch(matches) {

  let manOfTheMatches = matches.reduce((manOfTheMatches, match) => {
    if (manOfTheMatches.hasOwnProperty(match.season)) {

      if (manOfTheMatches[match.season].hasOwnProperty(match.player_of_match)) {
        manOfTheMatches[match.season][match.player_of_match] += 1;
      } else {
        manOfTheMatches[match.season][match.player_of_match] = 1;
      }
    }
    else {
      manOfTheMatches[match.season] = {};
    }
    return manOfTheMatches;
  }, {});
  
  let sortedOrderOfAwards = Object.values(manOfTheMatches)
      .map(nameAndNoOfAwards => Object.entries(nameAndNoOfAwards)
      .sort((current, previous) => current[1] - previous[1]));

  const topPlayers = sortedOrderOfAwards.map(nameAwards => nameAwards[nameAwards.length-1]);
 
  let mostAwardsPerYear =  matches.reduce((mostAwardsPerYear, match) => {
    mostAwardsPerYear[match.season] = 0;
    return mostAwardsPerYear;
}, {})

  
  Object.keys(mostAwardsPerYear).forEach((element,index) => {
    let name = topPlayers[index][0];
    let value = topPlayers[index][1];
    mostAwardsPerYear[element] = { [name]: value }
  })
 
  return mostAwardsPerYear;
}

const mostAwardsPerYear = playerOfTheMatch(matches);



module.exports = {
  matchesPerYear,
  noOfWinsPerYear,
  extraRuns2016,
  economicalBowlers,
  noOfTossAndMatches,
  playerOfTheMatch
};
